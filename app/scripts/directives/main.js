'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
    .directive('getWidth', function () {
        return {
            restrict: 'AE',
            link: function ($scope, elem) {
                
                var getWidth;

                function getWrapSize() {
                    getWidth = elem[0].offsetWidth;

                    elem.css({
                        height: getWidth
                    });
                };

                $(window).resize(getWrapSize);
                $(document).ready(getWrapSize);
            }
        }

    });

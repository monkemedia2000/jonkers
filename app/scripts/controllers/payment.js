'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:CartCtrl
 * @description
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('PaymentCtrl', function ($scope, $rootScope, $location, moltin) {
  	
    $scope.payment = function(data) {

      moltin.Checkout.Payment('purchase', $scope.order.id, {
      	data: $scope.data
      }, function(response) {
        // $rootScope.order = $rootScope.cart = null;
        $rootScope.payment = response;

		  var cartItems = $scope.order.id;
      
      moltin.Cart.Delete(cartItems);
        $rootScope.$apply(function() {
          $location.path('/complete');
        });
      });
    }
  });


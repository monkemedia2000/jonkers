'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('CheckoutCtrl', function ($scope, $rootScope, $location, moltin, cart, fields, options) {

    $scope.data = { 
      bill: {},
      ship: {}, 
      shipping: '', 
      gateway: ''
    }
    $scope.cart = cart;
    $scope.options = options;
    $scope.fields = fields;

    //Re-arrange countries in alphabetical order

    var countries = fields.country.available,
        sortable = [];

    for (var country in countries){
      sortable.push([country, countries[country]]);
    }       
    sortable.sort(function(a, b) {
      return a[1] - b[1]
    });

    $scope.sortable = sortable;

  $scope.createOrder = function(data) {
    
      moltin.Cart.Complete({
        customer: {
          first_name: $scope.data.ship.first_name,
          last_name:  $scope.data.ship.last_name,
          email:      $scope.data.ship.email_address
        },
        shipping: $scope.data.shipping,
        gateway: $scope.data.gateway,

        ship_to: $scope.data.ship,
        bill_to: $scope.data.ship

      }, function(response) {
        console.log(response);
        $rootScope.order = response;

        $rootScope.$apply(function() {
         $location.path('/payment');
         //$state.go('payment');
        });
      })
    }

  });

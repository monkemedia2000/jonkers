'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('HeaderCtrl', function ($scope, $rootScope, cart, $log, gettextCatalog) {
  	$rootScope.cart = cart;

    // Navigation fade hover
  	var $mainNav = $('.navigation-main'),
          $links = $mainNav.find('a');

    $links.mouseenter(function (e) {
        var $this = $(e.target);
        $mainNav.find('a').not($this).stop( true, true ).fadeTo('medium', 0.5);
    }).mouseleave('a', function () {
         $mainNav.find('a').fadeTo('medium', 1);
    });

    //SEARCH

    // $scope.test = function (data) {
    //   $scope.search = 'Bond'//$scope.data;

    //   console.log($scope.search)
      
    //   moltin.Product.Search({
    //     category: 'engagement rings',
    //     title: $scope.search,
    //     limit: 1
    //   }, function (products) {
    //     console.log(products)
    //     $scope.products = products
    //     $scope.$apply()
    //   });

    // }

  });

  // Navigation show on scroll
  function navShowOnScroll() {
    if ($(window).width() >= 992) {
      $(document).bind('scroll.myScroll', function(e){
        var scrollTop = $(document).scrollTop();
        if(scrollTop > 220){
          //console.log(scrollTop);
          $('.navbar').addClass('fixed');
          setTimeout(function () { 
            $('.navbar').addClass('animate');
          }, 50);

          $('.hero').addClass('set');
            
        } else {
          $('.navbar').removeClass('fixed animate');
          $('.hero').removeClass('set');
        }
      });
    } else {
      $('.navbar').removeClass('fixed');
      $('.navbar').removeClass('animate');
      $('.hero').removeClass('set');
      $('.navbar').unbind();
      $(document).unbind('.myScroll');
    }
  }

  $(window).resize(navShowOnScroll);
  $(document).ready(navShowOnScroll);
  

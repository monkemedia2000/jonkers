'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('CartCtrl', function ($scope, $rootScope, cart, moltin) {
  	$rootScope.cart = cart;

    console.log(cart)
  	
console.log(cart);
  	$scope.removeProd = function () {
      var itemId = this.id;

	  	moltin.Cart.Remove(itemId, function () {
		  	return moltin.Cart.Contents(function(cart) {
  				$rootScope.cart = cart;
  				$rootScope.$apply()
        });	        
      });
  	}
  });

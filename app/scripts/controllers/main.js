'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('MainCtrl', function ($scope, features, moltin) {
    $scope.features = features;

    // Full screen hero
    $(document).ready(function () {
    	
        var $navBar = $('.navbar'),
        	$hero = $('.hero');

        function makeFullScreen () {
        	var winHeight = $(window).height();

        	console.log(winHeight);

        	$hero.css({
        		height: winHeight
        	});
        }

        $(window).on('resize ready', function () {
        	makeFullScreen();
        })

        makeFullScreen();
    });

    // Product list height

    // $(window).resize(productListSize);
    // $(document).ready(productListSize)


    // function productListSize() {
    //     var getWidth;

    //     getWidth = $wrap.outerWidth();

    //     $wrap.css({
    //         height: getWidth
    //     });

        
    // }

  });

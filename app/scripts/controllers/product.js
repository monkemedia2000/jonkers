'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('ProductCtrl', function ($scope, $rootScope, moltin, $timeout, product, $localStorage) {

    var qty = 1,
        mod = {};

  	$scope.product = product[0];
    $scope.productId = $scope.product.id;
    $scope.productSlug = $scope.product.slug;
  	$scope.standard = true;
    $scope.modifiers = product[0].modifiers;

    function addError($this) {
      $this.parent('fieldset').addClass('error');
    }

    function removeError(el) {
      el.parent('fieldset').removeClass('error');
    }

    function positionTooltip($this) {
      $('.simple-mods .nya-bs-select').each(function () {
        var $this = $(this),
            width = $this.width(),
            leftPos = $this.position().left,
            tooltip = $this.next('.tooltip');

        tooltip.css({
          left: leftPos - 30
        });

        

        console.log(leftPos)
      });

    }

    function openContent() {
      var $readMore = $('.link a');

      $readMore.on('click', function () {
        var $this = $(this);

        if ($this.hasClass('more')) {
          $this.parents('.description').addClass('active');
        } else {
          $this.parents('.description').removeClass('active');
        }

        
      });
    }

    openContent();

    $scope.closeTip = function () {
      $(event.target).parents('fieldset').removeClass('error');
    }

    $scope.update = function () {
      $(event.target).parents('fieldset').removeClass('error');
    }

    $scope.addCart = function () {

      var select = $('.simple-mods .nya-bs-select'),
          selectedAmount = $('.simple-mods .nya-bs-select .selected a').length,
          selectAmount = select.length,
          selected = $('.simple-mods .nya-bs-select .selected a');

      if (selectAmount == selectedAmount) {

          selected.each(function () {
            mod[$(this).attr('ng-mod')] = $(this).attr('value');
          });

          addToCart();
      } else {
        
        select.each(function () {
          var getText;
          selected = $(this).find('.selected');

          if (selected.length < 1) {
            var $this = $(this);
            positionTooltip();
            addError($this);
            getText = $(this).attr('id');
          }
          
        });
      }

      function addToCart() {
  		  //$scope.addStatus = '<i class="fa fa-circle-o-notch fa-spin"></i>';
        $scope.standard = false;
        $scope.spinner = true;

    		moltin.Cart.Insert($scope.productId, qty, mod, function (cart) {
    			$scope.spinner = false;
          $scope.success = true;
    			$scope.$apply();
    			$timeout(function () {
    				$scope.success = false;
    			}, 3000).then(function () {
            $scope.standard = true;
          });

          

          $("html, body").animate({ scrollTop: 0 }, 150);

          moltin.Cart.Contents(function (cart) {
            $rootScope.cart = cart;
            $rootScope.$apply();
          });
    		});
      }
    }
  });

'use strict';

angular.module('storeApp')
  .controller('LangCtrl', function ($scope, $rootScope, gettextCatalog, $localStorage, Store) {
  	$scope.languages = ['en', 'nl'];
    $scope.langMod = 'en';

    if ($localStorage.message == undefined) {
      gettextCatalog.setCurrentLanguage('en');
      $scope.langMod = 'en';
      
    } else {
      gettextCatalog.setCurrentLanguage($localStorage.message);
      $scope.langMod = $localStorage.message;
    }

    $scope.changeLanguage = function () {
      var lang = $scope.langMod.toUpperCase(),
          currency;

      if (lang == 'EN') {
        currency = 'GBP'
      } else {
        currency = 'EUR'
      }

      Store.language(lang, currency);
      gettextCatalog.setCurrentLanguage($scope.langMod);
      $localStorage.message = $scope.langMod; 
    }
  });

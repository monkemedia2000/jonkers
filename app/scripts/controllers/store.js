'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:StoreCtrl
 * @description
 * # StoreCtrl
 * Controller of the storeApp
 */
angular.module('storeApp')
  .controller('StoreCtrl', function ($scope, $rootScope, products) {
    $rootScope.products = products;
  });

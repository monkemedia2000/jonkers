'use strict';

/**
 * @ngdoc function
 * @name storeApp.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the storeApp
 */
app.factory('Store', function ($q, MoltinAuth, $stateParams) {

  //Global
  return {
    cart: function () {
      var deferred = $q.defer();
      MoltinAuth.then(function(moltin) {
        moltin.Cart.Contents(function(cart) {
          deferred.resolve(cart);
        });
      })
      return deferred.promise;
    },

    language: function (lang, currency) {
      var deferred = $q.defer();
      MoltinAuth.then(function (moltin) {
        console.log(currency)
        moltin.Currency.Set(currency);
        moltin.Language.Set(lang, function () {
          location.reload();
        });
      });

      return deferred.promise;
    },

    // Home page
    features: function() {
      var deferred = $q.defer();
      MoltinAuth.then(function(moltin) {
        moltin.Product.List({collection:'1099880132488724748'}, function(collection) {
          deferred.resolve(collection);
          console.log(collection);
        });
      });
      return deferred.promise;
    },

    // Store
    products: function () {
      var deferred = $q.defer();
      $q.when(MoltinAuth).then(function (moltin) {
        moltin.Product.List({
        }, function (products) {
          deferred.resolve(products);
          console.log(products)
        });
      });

      return deferred.promise;
    },

    // PDP
    product: function(slug) {
      var deferred = $q.defer();
      MoltinAuth.then(function(moltin) {
        console.log(moltin);
          
        moltin.Product.Find({
          slug: slug
        }, function(product) {    
          deferred.resolve(product);
        });
        
      })
      return deferred.promise;
    },

    // Checkout

    options: function() {
      var deferred = $q.defer();
      MoltinAuth.then(function(moltin) {
        moltin.Cart.Checkout(function(options) {
          deferred.resolve(options);
        });
      })
      return deferred.promise;
    },

    fields: function() {
      var deferred = $q.defer();
      MoltinAuth.then(function(moltin) {
        moltin.Address.Fields(null, null, function(fields) {
          deferred.resolve(fields);
        });
      })
      return deferred.promise;
    }

  }
                
      
});


'use strict';

/**
 * @ngdoc overview
 * @name storeApp
 * @description
 * # storeApp
 *
 * Main module of the application.
 */
angular
  .module('storeApp.moltin', [])
    .factory('MoltinAuth', function($q) {
        var deferred = $q.defer();
        var moltin = new Moltin({publicId: 'cyELNXZ6O37teHJOAmNfIpqJ9Y07fQeyfRmIM9hB'});

  		moltin.Authenticate(function() { 
    		deferred.resolve(moltin);
		});

      return deferred.promise;
  });

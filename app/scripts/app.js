'use strict';

/**
 * @ngdoc overview
 * @name storeApp
 * @description
 * # storeApp
 *
 * Main module of the application.
 */
var app = angular
  .module('storeApp', [
    'storeApp.moltin',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'gettext',
    'ngStorage',
    'angularSimpleSpinner',
    'nya.bootstrap.select'
  ]);

  app.run(function (gettextCatalog, $rootScope, $location) {
    gettextCatalog.debug = true;
    
    $rootScope.$on('$stateChangeStart', function (event, next, current) {
      $('.navmenu').offcanvas('hide');
      $('.offcanvas.offcanvas-clone').detach();
      $('body').removeAttr('style');
    });

  });

  

  app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('home', {
        url: '/home',
        views: {
          'header': {
            templateUrl: 'partials/header-home.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              },
              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          },
          'content': {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            resolve: {
              features: function(Store) {
                return Store.features();
              },

              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })
      .state('store', {
        url: '/store',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function (Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/store.html',
            controller: 'StoreCtrl',
            resolve: {
              products: function (Store) {
                return Store.products();
              }
            }
          }
        } 
      })

      .state('product', {
        url: '/store/:slug/',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/product.html',
            controller: 'ProductCtrl',
            resolve: {
              product: function(Store, $stateParams, $rootScope) {
                var slug = $stateParams.slug;
                return Store.product(slug);
              },
              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })

      .state('cart', {
        url: '/cart',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/cart.html',
            controller: 'CartCtrl',
            resolve: {
              cart: function (Store) {
                return Store.cart();
              },
              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })

      .state('favourites', {
        url: '/favourites',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/favourites.html',
            controller: 'FavCtrl',
            resolve: {
              favourites: function (Store) {
                return Store.favourites();
              },
              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })

      .state('register', {
        url: '/register',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/register.html',
            controller: 'RegisterCtrl',
            resolve: {
              customers: function ($q, $stateParams, MoltinAuth) {
                var deferred = $q.defer();
                MoltinAuth.then(function(moltin) {
                  moltin.Customer.List(null, function(customers) {
                    deferred.resolve(customers);
                    console.log(customers);
                  });
                })
                return deferred.promise;
              }
            }
          }
        }
      })

      .state('checkout', {
        url: '/checkout',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function(Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/checkout.html',
            controller: 'CheckoutCtrl',
            resolve: {
              cart: function (Store) {
                return Store.cart();
              },
              options: function (Store) {
                return Store.options();
              },

              fields: function (Store) {
                return Store.fields();
              },
              
              moltin: function(MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })

      .state('payment', {
        url: '/payment',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function (Store) {
                return Store.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/payment.html',
            controller: 'PaymentCtrl',
            resolve: {
              cart: function (Store) {
                return Store.cart();
              },
              moltin: function (MoltinAuth) {
                return MoltinAuth;
              }
            }
          }
        }
      })

      .state('complete', {
        url: '/complete',
        views: {
          'header': {
            templateUrl: 'partials/header-default.html',
            controller: 'HeaderCtrl',
            resolve: {
              cart: function (myCart) {
                return myCart.cart();
              }
            }
          },
          'content': {
            templateUrl: 'views/complete.html',
            controller: 'CompleteCtrl'
          }
        }
      })

  });